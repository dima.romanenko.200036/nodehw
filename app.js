const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const app = express();
const port = 8080;
app.use(morgan('combined'));
app.use(express.json());

app.get('/api/files/:filename', (request, response) => {
  try {
    const filename = request.params.filename;
    fs.readFile(`./${request.url}`, 'utf8', (err, data) => {
      if (err)
        response.status(400).json({
          message: `No file with ${filename} filename found`,
        });
      else {
        fs.stat(`./${request.url}`, (err, stats) => {
          if (err) {
            response.status(400).json({
              message: `No file with ${filename} filename found`,
            });
          } else {
            response.status(200).json({
              message: 'Success',
              filename: filename,
              content: data,
              extension: filename.split('.')[filename.split('.').length-1],
              uploadedDate: stats.mtime,
            });
          }
        });
      }
    });
  } catch (e) {
    response.status(500).json({
      message: 'Server error',
    });
  }
});
app.get('/api/files', (request, response) => {
  try {
    fs.readdir(`.${request.url}`, (err, files) => {
      if (err) {
        response.status(400).json({
          message: 'Client error',
        });
      } else {
        response.status(200).json({
          message: 'Success',
          files: files,
        });
      }
    });
  } catch (e) {
    response.status(500).json({
      message: 'Server error',
    });
  }
});
app.post('/api/files', (request, response) => {
  try {
    const filename = request.body.filename;
    const content = request.body.content;
    if (filename && content) {
      fs.writeFile(`.${request.url}/${filename}`, content, (err) => {
        if (err) {
          response.status(400).json({
            message: 'Please specify `filename` parameter',
          });
        } else {
          response.status(200).json({ message: `File created successfully` });
        }
      });
    } else {
      if (!content)
        response.status(400).json({
          message: 'Please specify `content` parameter',
        });
      else if (!filename)
        response.status(400).json({
          message: 'Please specify `filename` parameter',
        });
      else response.status(400).json({
          message: 'Please specify `filename` && `content` parameters',
      });  
    }
  } catch (e) {
    response.status(500).json({
      message: 'Server error',
    });
  }
});

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }
  console.log(`server is listening on ${port}`);
});
